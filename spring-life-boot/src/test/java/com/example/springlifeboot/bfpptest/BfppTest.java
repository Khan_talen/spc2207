package com.example.springlifeboot.bfpptest;

import com.example.springlifeboot.bfppdemo.MyConfig;
import com.example.springlifeboot.bfppdemo.UserService;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

/**
 * 测试BFPP修改作用域效果
 */
public class BfppTest {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(MyConfig.class);
        UserService userService = context.getBean(UserService.class);
        UserService userService2 = context.getBean(UserService.class);
        System.out.println(userService2==userService);

    }

}
