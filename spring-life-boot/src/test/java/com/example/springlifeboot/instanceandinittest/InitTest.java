package com.example.springlifeboot.instanceandinittest;

import com.example.springlifeboot.instanceandinit.InstanceAndInitConfig;
import com.example.springlifeboot.instanceandinit.PersonController;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 测试三种注入的顺序
 */
public class InitTest {
    public static void main(String[] args) throws InterruptedException {
        //spring容器启动了，所有的Bean都已经实例化并初始化好了
        ConfigurableApplicationContext context = SpringApplication.run(InstanceAndInitConfig.class);
//        PersonController personController = context.getBean(PersonController.class);
//        PersonController personController1 = context.getBean(PersonController.class);

        context.close();
//        System.exit(0);  //JVM
    }
}
