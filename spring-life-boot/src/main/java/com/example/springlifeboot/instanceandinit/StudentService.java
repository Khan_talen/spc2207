package com.example.springlifeboot.instanceandinit;

import org.springframework.stereotype.Service;

public class StudentService {

    void stuInit(){
        System.out.println("StudentService初始化操作。。。。");
    }

    void stuDes(){
        System.out.println("StudentService实例销毁前自定义操作");
    }
}
