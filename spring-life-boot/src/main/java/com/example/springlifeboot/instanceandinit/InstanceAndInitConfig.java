package com.example.springlifeboot.instanceandinit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 通过@Bean注解的属性实现初始化和销毁调用方法
 */
@Configuration
@ComponentScan("com.example.springlifeboot.instanceandinit")
public class InstanceAndInitConfig {

    @Bean(initMethod = "stuInit",destroyMethod = "stuDes")
//    @Bean
    public StudentService studentService(){
        return new StudentService();
    }
}
