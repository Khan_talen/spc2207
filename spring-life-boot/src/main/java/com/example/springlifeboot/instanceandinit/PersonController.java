package com.example.springlifeboot.instanceandinit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 构造注入，setter注入和字段注入的顺序
 */
@Controller
@Scope("prototype")
public class PersonController {

    @Autowired
    PersonService personService;

    /**
     * 构造方法注入
     */
    StudentService studentService;

    public PersonController(StudentService studentService){
        this.studentService = studentService;
        System.out.println("PersonController构造注入。。。。。。");
        System.out.println(personService);  //查看字段注入是否完成
    }

    /**
     * setter注入
     */
    WorkerService workerService;
    @Autowired
    public void setWorkerService(WorkerService workerService){
        System.out.println(personService);  //字段注入的值
        this.workerService = workerService;
        System.out.println("PersonController通过setter注入。。。。。");
    }

    @PostConstruct
    void myInit(){
        System.out.println("PersonController初始化操作。。。。");
    }
    @PreDestroy
    void myDes(){
        System.out.println("PersonController实例销毁前自定义操作,通常用于释放资源");
    }
}
