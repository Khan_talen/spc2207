package com.example.springlifeboot.instanceandinit;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * BPP会在所有bean的初始化器调用之前和之后执行
 */
@Component
public class MyBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(beanName+"的BPP的beforeInit方法执行。。。。。。");
        return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(beanName+"的BPP的afterInit方法执行。。。。。。");
        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }
}
