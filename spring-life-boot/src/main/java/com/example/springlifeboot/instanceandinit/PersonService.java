package com.example.springlifeboot.instanceandinit;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class PersonService {
    /**
     *  @PostConstruct是在Bean初始化过程中，执行一些自定义操作
     *  Bean的实例化过程中
     *  依赖注入会发生在bean的实例化过程中
     *  该注解作用的方法实在依赖注入之后执行的
     */
//    @PostConstruct
//    public void myInit(){
//        System.out.println("PersonService中的自定义初始化操作。。。。。");
//    }

    /**
     * Spring管理的Bean的销毁：
     * Spring容器启动，Bean已经创建好，Bean默认是单例的，会存在于SPring容器的整个
     * 生命周期中，即当Spring容器关闭前，Bean会被销毁
     * 添加了@PreDestroy注解的方法会在Bean销毁之前调用
     * 10：05上课
     *
     */
//    @PreDestroy
//    public void myDes(){
//        System.out.println("PersonService中的自定义销毁操作。。。。。。");
//    }

}
