package com.example.springlifeboot.bfppdemo;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

/**
 *  BFPP修改Bean定义，修改作用域原理
 */
public class MyBFPP implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory factory) throws BeansException {
        //修改UserService的作用域
        //从BeanFactory中获取对应的Bean定义
        BeanDefinition definition = factory.getBeanDefinition("userService");
        definition.setScope("prototype");

    }
}
