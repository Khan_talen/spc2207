package com.example.springlifeboot.bfppdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class MyConfig {

    @Bean
    public static MyBFPP myBFPP(){
        return new MyBFPP();
    }

    @Bean
    public UserService userService(){
        return new UserService();
    }

}
