package com.example.springlifeboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLifeBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLifeBootApplication.class, args);
    }

}
