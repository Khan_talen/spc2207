#### SpringJdbc

- 定义

  ~~~java
  是对传统JDBC的封装，解决了传统JDBC冗余，易错，以及异常处理的问题。采用模板设计模式
  ~~~

- 使用SpringJdbc的步骤

  - 导入相关依赖

    ~~~java
    1. mysql驱动依赖
    	    <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
            </dependency>
    2. 阿里的连接池依赖
    	<dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>druid</artifactId>
                <version>1.1.21</version>
            </dependency>
    3. spring-jdbc依赖
    	<dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-jdbc</artifactId>
            </dependency>
    ~~~

  - 在属性文件配置数据源信息

    ~~~java
    spring:
      datasource:
        url: jdbc:mysql://localhost:3306/spring_test?characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai&rewriteBatchedStatements=true
        username: root
        password: root
    ~~~

    

- 疑问：SpringJdbc什么时候用

  ~~~java
  SpringJdbc是持久层框架
  Spring全家桶     衍生框架   --SpringMvc -- 作用在web层
      					  -- SpringJdbc 持久层框架
  持久层框架：
      mybatis  -- 大部分项目都用mybatis
      	mybatis是重量级的框架，内部用到了反射，执行效率没有SpringJDBC快
      	但是用起来非常方便
      SpringJdbc -- 适用于小的轻量级的项目  
      	是轻量级的框架，实际上就是对传统JDBC的封装，提供了对异常的处理，执行效率和传统JDBC一样，非常快，Nacos底层用SpringJDBC做的
  ~~~

- 模板类 -- JdbcTemplate

  ~~~java
  SpringJDBC完成的操作有哪些？
      1. 获取连接
  	2. 参与事务
  	3. 执行sql
  	4. 处理结果集
  	5. 处理异常
  	6. 释放连接
  程序员只需要调用该类中的方法即可完成相对应的操作，不用再写模板代码，不用考虑模板代码中的异常处理，操作方便了很多。
  ~~~

- JdbcTemplate的使用

  - 测试导入依赖后，JdbcTemplate是否可以正常使用

    ~~~java
    测试从JdbcTemplate中获取连接对象，看是否获取成功，若能获取成功连接对象，则该模板类可以使用
    在测试类中测试：
    @SpringBootTest
    class SpringJdbcTxHascontent2205ApplicationTests {
        @Autowired
        JdbcTemplate jdbcTemplate;
    
        @Test
        void contextLoads() throws SQLException {
            System.out.println(jdbcTemplate.getDataSource().getConnection());
        }
    }
    ~~~

  - 增删改操作

    ~~~java
  update(sql,Object...args)  
        
    ~~~
  
  日期类型和String类型间的转换
        String-->LocalDate
        	 DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDate birthDate = LocalDate.parse("1998-12-12",formatter);
            System.out.println(birthDate);
  
    	LocalDate-->String
            String currentDate = formatter.format(LocalDate.now());
            System.out.println(currentDate);
  
  
  
- 查询操作

  - 查询简单类型 -- int  long   String   LocalDate。。。。。。

    ~~~
   queryForObject(sql,Class returnType)
     1. 查询学生总量
     2. 查询所有学生中最小的出生日期
    ~~~

    - Generic Maps  -- 了解即可，基本不用

      ~~~java
    查询的结果以键值对保存在map中，不会将数据封装入对象中，可读性差，了解即可。
      queryForMap(sql,Object...args) -- Map<k,v>
    queryForList(sql,Object...args) - List<Map<k,v>>
      ~~~

    - domain  Object -- （领域）对象

      ~~~java
    JdbcTemplate不会自动将resultset中数据封映射到对象中，不会帮你完成自动映射，需要手动映射
      通过实现接口RowMapper完成结果集到对象的映射
        
      1. 查询返回某个对象 -- queryForObject
          因为在这里需要程序员手动完成映射，所以需要使用以下的这个方法
          	queryForObject(sql,RowMapper,Object...args)
          public Student findStudentById(Integer id) {
              String sql = "select * from student where id=?";
              return jdbcTemplate.queryForObject(sql, new RowMapper<Student>() {
                  @Override
                  public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
                      Student student = new Student();
                      student.setId(rs.getInt("id"));
                      student.setName(rs.getString("name"));
                      Date date = rs.getDate("birth_date");
                      //student类中的日期要求类型为LocalDate类型，而从数据库查询出的类型为java.sql.Date
                      //将java.sql.Date转换LocalDate类型
                      LocalDate birthDate = date.toLocalDate();
                      student.setBirthDate(birthDate);
                      return student;
                  }
              }, id);
          }
           
      2. 查询返回list集合
          query(sql,RowMapper<T>) -- List<T>
      ~~~

##### 异常处理

- 异常分类：

  ~~~java
  Exception分成2类：
      检查异常：在编译过程中能够检查到的异常，必须处理，否则编译不通过
      非检查异常/运行时异常：NullPointerException
      	在编译过程中检测不到，在运行过程中会出现的异常，叫做运行时异常
  ~~~

#### 事务

- 定义

  ~~~java
  事务是数据库中执行操作的最小执行单元，不可再分，要么全都执行成功，要么全都执行失败。
  ~~~

- 特性

  ~~~java
  1. 原子性
  2. 一致性
  3. 隔离性 
  4. 持久性 
  ~~~

- 传统Java事务管理的问题

  ~~~java
  1. 持久层框架不同，事务使用的api不同的
  2. 当持久层框架确定后，手动管理事务，使用api方法开启事务，事务提交，事务回滚，代码重复
  3. 局部事务管理和分布式事务管理使用api不同
  ~~~

- Spring事务管理

  ~~~java
  1. 解决了持久层框架不同，使用api不同的问题--解决办法是通过平台事务管理器
  2. 使用aop解决了代码重复问题，将事务管理的关注点代码提取到切面中，在运行期动态的进行织入
  3. 解决了全局事务和局部事务使用不同api的问题
      	通过事务管理器解决了以上问题
      	且全局事务和局部事务中，事务失败后，给出的处理方式是一样的
  ~~~

- Spring声明式事务

  ~~~java
  提供了注解@Transactional，来进行事务管理
  添加上该注解，开启事务管理的机制：
      在运行期，调用某个目标业务方法时，若检测到方法上有该注解，则开启事务管理，将切面中的通知代码织入到目标业务代码中，事务管理的通知是环绕通知。
  ~~~

  @Transactional注解的用法：

  ~~~java
  可以用于以下位置：
      1. 业务方法上方
      2. 业务层接口上方 -- 接口中的所有方法都开启事务管理 -- 从Spring5.0开始的
      3. 业务类上方 -- 类中所有的方法都开启事务管理
  ~~~

- 注意点：在SPring项目中（不是SpringBoot）,若要开启事务管理，必须在配置类的上方加注解**@EnableTransactionManagement**

- 声明式事务管理

  - 采用的aop，在运行期会生成代理对象
  - 业务方法中抛出运行时异常，则事务回滚；若抛出的是检查异常，事务不会回滚

- @Transactional注解的属性

  ~~~java
  timeout:  指定超时时间，单位s  默认值-1，表示永不超时
  若同时在类上方和某个方法上方指定超时时间，则添加了该属性的方法的超时时间以方法上的时间为准，这里的现象是属性覆盖
  ~~~

##### 事务传播

- 概念

  ~~~java
  2个业务方法(至少有一个开启事务管理)之间存在调用现象，此时就会涉及到事务传播。
  ~~~

- 传播级别 -- 面试题 

  ~~~java
  属性：propagation
  属性值：7个
      required:  默认的    当前无事务，则新建事务，当前有事务，则延用当前事务
      requires_new:   当前无事务，创建新事务；当前有事务，暂停当前事务，创建新事务
      mandatory:强制的  当前存在事务，使用当前事务，当前无事务，抛出异常
  	never：始终在无事务情况下执行，当前无事务，继续在无事务状态下执行，若当前有事务，抛出异常
  	not_supported:不使用事务，当前无事务，继续在无事务状态下执行，若当前有事务，则暂停当前事务，在无事务状态下执行
  	supports:支持，当前无事务，继续无事务 ；当前存在事务，使用当前事务
  	nested：嵌套的，当前无事务，创建新事务，当前存在事务，创建新的内嵌事务       
  ~~~

- 事务传播注意点：

  ~~~java
  事务传播一定会发生在跨代理对象调用
  ~~~

##### 事务的回滚规则

~~~java
1.默认情况下，业务方法抛出运行时异常，事务回滚
2.可以自定义回滚规则，通过属性rollbackFor和noRollBackFor来指定
~~~

##### @Transacational注解作用在集成测试中特点

- 该注解可以作用在测试方法/类上方，但是执行之后会自动回滚，避免测试之后对数据进行清理。













