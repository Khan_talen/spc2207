package com.example.springjdbctx.exception;

public class RegistFaildException extends RuntimeException{

    public RegistFaildException() {
    }

    public RegistFaildException(String message) {
        super(message);
    }

    public RegistFaildException(String message, Throwable cause) {
        super(message, cause);
    }

    public RegistFaildException(Throwable cause) {
        super(cause);
    }

    public RegistFaildException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
