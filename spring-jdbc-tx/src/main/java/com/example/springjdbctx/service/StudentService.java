package com.example.springjdbctx.service;


import com.example.springjdbctx.entity.Student;

public interface StudentService {

    Student regist(Student student);

    void deleteStudentById(Integer id);
}
