package com.example.springjdbctx.service.impl;

import com.example.springjdbctx.dao.StudentDao;
import com.example.springjdbctx.entity.Student;
import com.example.springjdbctx.exception.DeleteFaildException;
import com.example.springjdbctx.exception.IllegalParameterException;
import com.example.springjdbctx.exception.RegistFaildException;
import com.example.springjdbctx.exception.UserNameIsUsedException;
import com.example.springjdbctx.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 事务相关测试
 */
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentDao studentDao;

    /**
     * 测试timeout属性
     * @param student
     * @return
     */
    @Transactional(timeout = 1)
    public Student regist(Student student) {
        if (student==null || student.getName()==null) {
            throw new IllegalParameterException("参数异常！");
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Student stu = studentDao.findStudentByName(student.getName());
        if (stu!=null) {
            throw new UserNameIsUsedException("该账号已经被注册！");
        }
        int i = studentDao.saveStudent(student);
        if (i!=1) {
            throw new RegistFaildException("注册账号失败！");
        }
        return stu;
    }

    /**
     * 测试方法中@Transactional注解事务的回滚功能
     * @param id
     */
    @Override
    public void deleteStudentById(Integer id) {
        int row = studentDao.deleteStudent(id);
        if (row!=1){
            throw new DeleteFaildException("删除学生信息失败！");
        }
    }


}
