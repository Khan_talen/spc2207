package com.example.springjdbctx.dao;




import com.example.springjdbctx.entity.Student;

import java.time.LocalDate;
import java.util.List;

public interface StudentDao {

    int saveStudent(Student student);

    int deleteStudent(Integer id);

    int updateStudent(Student student);

    int findStudentNum();

    LocalDate findMinBirthDate();

    Student findStudentById(Integer id);

    List<Student> findAll();


    Student findStudentByName(String name);
}
