package com.example.springjdbctx.dao.impl;

import com.example.springjdbctx.dao.StudentDao;
import com.example.springjdbctx.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

/**
 * JdbcTemplate测试
 */
@Repository
public class StudentDaoImpl implements StudentDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * jdbcTemplate测试添加数据
     * @param student
     * @return
     */
    public int saveStudent(Student student){
        String sql = "insert into student values(null,?,?)";
        return jdbcTemplate.update(sql,student.getName(),student.getBirthDate());
    }


    /**
     * jdbcTemplate测试删除数据
     * @param id
     * @return
     */
    public int deleteStudent(Integer id){
        String sql = "delete from student where id=?";
        return jdbcTemplate.update(sql,id);
    }

    /**
     * jdbcTemplate测试修改数据
     * @param student
     * @return
     */
    public int updateStudent(Student student){
        String sql = "update student set name=?,birth_date=? where id=?";
        return jdbcTemplate.update(sql,student.getName(),student.getBirthDate(),student.getId());
    }

    /**
     * jdbcTemplate测试查询，返回int
     * @return
     */
    public int findStudentNum(){
        String sql = "select count(*) from student";
        return jdbcTemplate.queryForObject(sql,Integer.class);
    }

    /**
     * jdbcTemplate测试查询，返回日期类型
     * @return
     */
    public LocalDate findMinBirthDate(){
        String sql = "select min(birth_date) from student";
        return jdbcTemplate.queryForObject(sql,LocalDate.class);
    }

    RowMapper<Student> rowMapper = (rs,rowNum)->{
        Student student = new Student();
        student.setId(rs.getInt("id"));
        student.setName(rs.getString("name"));
        Date birthDate = rs.getDate("birth_date");
        LocalDate localDate = birthDate.toLocalDate();
        student.setBirthDate(localDate);
        return student;
    };

    /**
     * jdbcTemplate测试查询，返回Domain Object
     * @param id
     * @return
     */
    @Override
    public Student findStudentById(Integer id) {
        String sql = "select * from student where id=?";
        return jdbcTemplate.queryForObject(sql, rowMapper, id);
    }

    /**
     * jdbcTemplate测试查询,返回集合
     * @return
     */
    @Override
    public List<Student> findAll() {
        String sql = "select * from student";
        return jdbcTemplate.query(sql, rowMapper);
    }

    /**
     * 后续测试需要
     * @param name
     * @return
     */
    @Override
    public Student findStudentByName(String name) {
        String sql = "select * from student where name=?";
        return null;
    }


}
