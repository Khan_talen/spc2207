package com.example.springjdbctx.service;

import com.example.springjdbctx.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

/**
 * 事务相关测试
 */
@SpringBootTest
public class StudentServiceTest {

    @Autowired
    StudentService studentService;

    /**
     * 测试timeout属性
     */
    @Test
    public void registTest(){
        Student student = new Student(null,"lisa",LocalDate.now());
        Student student1 = studentService.regist(student);
    }

    /**
     * 测试事务作用在测试方法上方，执行结束，自动回滚
     */
    @Test
//    @Transactional
    public void deleteTest(){
        studentService.deleteStudentById(3);
    }
}
