package com.example.springjdbctx.dao;

import com.example.springjdbctx.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * JdbcTemplate测试
 */
@SpringBootTest
public class StudentDaoTest {

    @Autowired
    StudentDao studentDao;

    @Test
    public void DateStringFormatTest(){
        /**
         * java中的日期类型  LocalDate和String之间的类型转换
         * 日期转换需要使用日期转换类   DateTimeFormatter
         * 通过日期转换类来完成转换
         */
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
//        LocalDate birthDate = LocalDate.parse("1998-12-12",formatter);
//        System.out.println(birthDate);

        /**
         * LocalDate-->String  同样需要使用日期转换类对象
         * formatter.format(LocalDate.now());
         */
        String currentDate = formatter.format(LocalDate.now());
        System.out.println(currentDate);
    }


    @Test
    public void saveStudentTest(){
        //创建Student对象
        Student student = new Student();
        student.setName("王五");
        //1998-12-12  LocalDate   日期类型转换  String-->LocalDate
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birthDate = LocalDate.parse("1996-08-12",formatter);
        student.setBirthDate(birthDate);
        //调用dao中得save方法执行，是否测试成功
        int row = studentDao.saveStudent(student);
        System.out.println(row);
    }

    @Test
    public void deletStudentTest(){
        int row = studentDao.deleteStudent(5);
        System.out.println(row);
    }

    @Test
    public void updateStudentTest(){
        Student student = new Student();
        student.setId(3);
        student.setName("李四");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse("1999-01-01",formatter);
        student.setBirthDate(date);

        int row = studentDao.updateStudent(student);
        System.out.println(row);
    }

    @Test
    public void findStudentNumTest(){
        int count = studentDao.findStudentNum();
        System.out.println(count);
    }

    @Test
    public void findMinBirthDateTest(){
        LocalDate localDate = studentDao.findMinBirthDate();
        System.out.println(localDate);
    }

    @Test
    public void findStudentById(){
        Student student = studentDao.findStudentById(1);
        System.out.println(student);
    }

    @Test
    public void findAllTest(){
        List<Student> list = studentDao.findAll();
        list.forEach(student -> System.out.println(student));
    }



}
