package com.example.springmvcrestful.controller;

import com.example.springmvcrestful.Entity.User;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 演示传参的3种方式
 */
@RestController
@RequestMapping("/users")
public class UserController {

    public List<User> findUsers(){
        List<User> list = new ArrayList<>();
        for (int i=0;i<3;i++){
            User user = new User(i,"tom"+i,10+i);
            list.add(user);
        }
        return list;
    }

    /**
     * 演示键值对传参
     * @param name
     * @param pwd
     * @return
     */
//    @GetMapping("/login")
    @PostMapping("/login")
    public String login(String name, String pwd){
        System.out.println(name+"--"+pwd);
        return "登录成功！";
    }
    /**
     * 演示json传参
     */
    @PostMapping("/regist")
    public String regist(@RequestBody User user){
        System.out.println(user);
        return "注册成功!";
    }


    /**
     * 演示路径传参
     */
    @GetMapping("/deleteUserById/{id}")
    public String delteUserById(@PathVariable Integer id){
        System.out.println(id);
        return "删除成功！";
    }



}
