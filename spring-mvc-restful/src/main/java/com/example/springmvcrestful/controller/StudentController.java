package com.example.springmvcrestful.controller;

import com.example.springmvcrestful.Entity.Student;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * 演示RestFul风格编程
 */
@RestController
@RequestMapping(value = "/student")
public class StudentController {

    /**
     * 下载excle文件
     */
    @GetMapping("/download")
    public ResponseEntity<byte[]> download(){
        //模拟文件中展示的学生列表数据
        List<Student> list = new ArrayList<>();
        for (int i=0;i<20;i++){
            Student student = new Student(i+1,"stu"+(i+1));
            list.add(student);
        }
        //将以上数据转换为字节 String :id,name\n1,tom\n2,jack -->String: getByte()
        //将列名存入builder
        StringBuilder builder = new StringBuilder("id,name");
        list.forEach(student -> builder.append("\n").append(student.getId())
                .append(",").append(student.getName()));
        //得到字节数组，将来向响应体中存入
        byte[] bys = builder.toString().getBytes(StandardCharsets.UTF_8);
        /*
        向响应header部分添加属性：
        Content-Type：application/csv
        Content-Length:bys.length
        Content-Disposition:attachment;filename=\"fname.csv\"
         */
        MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type","application/csv");
        headers.add("Content-Length",bys.length+"");
        headers.add("Content-Disposition","attachment;filename=\"fname.csv\"");

        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(bys,headers, HttpStatus.OK);
        return responseEntity;
    }



}
