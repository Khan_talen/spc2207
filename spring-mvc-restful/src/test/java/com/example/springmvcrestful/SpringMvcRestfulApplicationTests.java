package com.example.springmvcrestful;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.converter.HttpMessageConverter;

@SpringBootTest
class SpringMvcRestfulApplicationTests {
    @Autowired
    ApplicationContext context;

    /**
     * 测试SpringBoot自动配置好的HttpMessageConvert有哪些
     */
    @Test
    void contextLoads() {
        String[] names = context.getBeanNamesForType(HttpMessageConverter.class);
        for (String name:names){
            System.out.println(name);
        }
    }


}
