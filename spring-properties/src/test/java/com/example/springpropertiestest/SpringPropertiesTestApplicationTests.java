package com.example.springpropertiestest;

import com.example.springpropertiestest.configuration.DevConfig;
import com.example.springpropertiestest.configuration.TestConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;

@SpringBootTest
class SpringPropertiesTestApplicationTests {

    @Autowired
    DevConfig devConfig;

//    @Autowired
//    TestConfig testConfig;

    @Test
    void test(){
        System.out.println(devConfig);
//        System.out.println(testConfig);
    }

}
