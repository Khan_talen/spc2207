package com.example.springpropertiestest.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person")
public class PersonController {

//    @Value("${my.test.pageNum}")
//    int pageNum;
//
//    @Value("${my.test.num}")
//    int num;
//
//    @Value("${my.test}")
//    int testNum;
//
//    @Value("${pageTotal}")
//    int pageTotal;
//
    @Value("${my.name}")
    String name;

    @GetMapping("/find")
    public String find(){
//        System.out.println("pageNum="+pageNum);
//        System.out.println("num="+num);
//        System.out.println("testNum="+testNum);
//        System.out.println("pageTotal="+pageTotal);
        System.out.println("name="+name);
        return null;
    }
}
