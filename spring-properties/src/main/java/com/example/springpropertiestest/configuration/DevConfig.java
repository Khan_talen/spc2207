package com.example.springpropertiestest.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * 激活dev环境测试对应的配置类Bean是否被加载到容器
 */
@Configuration
@Profile("dev")
public class DevConfig {
}
