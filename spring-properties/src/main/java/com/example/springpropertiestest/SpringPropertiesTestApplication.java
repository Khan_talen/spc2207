package com.example.springpropertiestest;

import com.example.springpropertiestest.configuration.DevConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
@SpringBootApplication
//@PropertySource("classpath:myapp3.properties")
//@PropertySource("file:spring-properties/myapp2.properties")
public class SpringPropertiesTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringPropertiesTestApplication.class, args);
    }

}
