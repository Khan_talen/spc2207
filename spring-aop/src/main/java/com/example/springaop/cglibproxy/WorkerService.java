package com.example.springaop.cglibproxy;

public class WorkerService {
    public String regist(){
        //模拟注册的业务
        System.out.println("开始注册。。。。");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("注册成功。。。。");
        return "注册成功！";
    }

}
