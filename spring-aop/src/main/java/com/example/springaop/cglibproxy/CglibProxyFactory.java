package com.example.springaop.cglibproxy;

import org.springframework.cglib.proxy.Enhancer;

public class CglibProxyFactory {
    public static void main(String[] args) {
        //创建目标对象
        WorkerService service = new WorkerService();
        //创建增强对象
        CglibTimeAspect aspect = new CglibTimeAspect(service);
        //使用cglib的api生成代理对象
        WorkerService proxy = (WorkerService) Enhancer.create(service.getClass(),aspect);
        //通过代理对象调用目标业务方法，观察结果
        String mes = proxy.regist();
        System.out.println("返回值："+mes);
        //查看生成的代理对象的类型
        System.out.println(proxy.getClass());
    }
}
