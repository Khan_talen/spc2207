package com.example.springaop.cglibproxy;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CglibTimeAspect implements MethodInterceptor {
    private Object target;

    public CglibTimeAspect(Object target){
        this.target = target;
    }

    /**
     *
     * @param proxy   代理对象
     * @param method  拦截到的方法
     * @param args   拦截到的方法的参数列表
     * @param methodProxy 方法的代理对象
     * @return
     * @throws Throwable
     */
    @Override
    public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        long beforeTime = System.currentTimeMillis();
        //执行业务代码
        Object returnVal = method.invoke(target,args);
        long afterTime = System.currentTimeMillis();
        System.out.println(method.getName()+"耗时："+(afterTime-beforeTime)+"毫秒");
        return returnVal;
    }


}
