package com.example.springaop.jdkproxy;

public class UserServiceImpl implements UserService{
    @Override
    public void login(String userName, String pwd) {
        System.out.println("开始登录！");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("登录成功！");
    }
}
