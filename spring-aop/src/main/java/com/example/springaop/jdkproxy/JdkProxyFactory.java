package com.example.springaop.jdkproxy;

import java.lang.reflect.Proxy;

public class JdkProxyFactory {
    public static void main(String[] args) {
        //创建目标对象
        UserService userService = new UserServiceImpl();
        //创建增强对象
        TimeAspect timeAspect = new TimeAspect(userService);
        //使用JDK的api将二者在运行期组合到一起，产生新的代理对象
        UserService proxy = (UserService) Proxy.newProxyInstance(
                userService.getClass().getClassLoader(),
                userService.getClass().getInterfaces(),
                timeAspect);
        proxy.login("tom","123");
        System.out.println(proxy.getClass());

    }
}
