package com.example.springaop.jdkproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class TimeAspect implements InvocationHandler {
    private Object target; //目标对象

    public TimeAspect(Object target){
        this.target = target;
    }

    /**
     * 1. 获取当前时间
     * 2. 目标业务方法执行
     * 3. 获取当前时间，得到时间差
     * method: 运行期拦截到的方法
     * args：目标方法的参数列表
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        long t1 = System.currentTimeMillis();
        Object returnVal = method.invoke(target,args);
        long t2 = System.currentTimeMillis();
        System.out.println(method.getName()+"耗时："+(t2-t1)+"毫秒");
        return returnVal;
    }


}
