package com.example.springaop.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class TimeAspect {
    //切入点表达式外部化
    @Pointcut("execution(* *..service.*.*(..))")
    public void pointcutMethod(){}

    /**
     * 环绕通知注意事项：
     * 1. 参数必须为ProceedingJoinPoint
     * 2. 返回值必须将目标业务方法的返回值返回
     * 3. 目标业务方法的异常需要抛出去（若捕获，则停止了异常的传播）
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around("pointcutMethod()")
    public Object getTime(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("环绕通知开始。。。");
        long beforeTime = System.currentTimeMillis();
        //接受目标业务方法的返回值，本方法中最后返回
        Object returnVal = joinPoint.proceed();
        long afterTime = System.currentTimeMillis();
        System.out.println(joinPoint.getSignature()+"耗时："+(afterTime-beforeTime)+"毫秒");
        System.out.println("环绕通知结束。。。。");
        return returnVal;
    }

//    @Before("pointcutMethod()")
//    public void log(JoinPoint joinPoint){
//        System.err.println("前置通知。。。。。");
//        System.out.println("getThis()="+joinPoint.getThis());
//        System.out.println("getTarget()="+joinPoint.getTarget());
//        System.out.println("getSignature()="+joinPoint.getSignature());
////        int i = 10/0;
//    }

//    @After("pointcutMethod()")
//    public void afterAdvice(){
//        System.out.println("后置通知。。。");
//    }

//    @AfterReturning(value = "pointcutMethod()",returning = "list")
//    public void afterReturningAdvice(List<Person> list){
//        System.out.println(list.size());
//        System.out.println("返回值后通知。。。");
//    }

//    @AfterThrowing(value = "pointcutMethod()",throwing = "e")
//    public void afterThrowingAdvice(JoinPoint joinPoint,UserNotFoundException e){
//        System.out.println("抛出异常后通知。。。");
//        throw new RuntimeException("用户不存在异常。。。。");
//    }





}
