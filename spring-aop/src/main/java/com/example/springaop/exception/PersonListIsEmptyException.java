package com.example.springaop.exception;

public class PersonListIsEmptyException extends RuntimeException{

    public PersonListIsEmptyException() {
    }

    public PersonListIsEmptyException(String message) {
        super(message);
    }

    public PersonListIsEmptyException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersonListIsEmptyException(Throwable cause) {
        super(cause);
    }

    public PersonListIsEmptyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
