package com.example.springaop.exception;

public class UserNameIsUsedException extends RuntimeException{

    public UserNameIsUsedException() {
    }

    public UserNameIsUsedException(String message) {
        super(message);
    }

    public UserNameIsUsedException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserNameIsUsedException(Throwable cause) {
        super(cause);
    }

    public UserNameIsUsedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
