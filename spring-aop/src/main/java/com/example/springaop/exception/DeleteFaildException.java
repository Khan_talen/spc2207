package com.example.springaop.exception;

public class DeleteFaildException extends RuntimeException{
    public DeleteFaildException() {
    }

    public DeleteFaildException(String message) {
        super(message);
    }

    public DeleteFaildException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeleteFaildException(Throwable cause) {
        super(cause);
    }

    public DeleteFaildException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
