package com.example.springaop.exception;

public class UpdateFaildException extends RuntimeException{
    public UpdateFaildException() {
    }

    public UpdateFaildException(String message) {
        super(message);
    }

    public UpdateFaildException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpdateFaildException(Throwable cause) {
        super(cause);
    }

    public UpdateFaildException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
