package com.example.springaop.service;


import com.example.springaop.entity.Person;

import java.util.List;

public interface PersonService {
    void regist(Person person);

    Person login(String personName, String password);

    List<Person> list();

    void deleteById(Integer id);
}
