package com.example.springaop.service.impl;

import com.example.springaop.dao.PersonDao;
import com.example.springaop.entity.Person;
import com.example.springaop.exception.*;
import com.example.springaop.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    PersonDao personDao;

    @Override
    public void regist(Person person) {
        if (person ==null || person.getName()==null || person.getPassword()==null) {
            throw new IllegalParameterException("参数异常！");
        }

        Person findUser = personDao.findPersonByName(person.getName());
        if (findUser!=null) {
            throw new UserNameIsUsedException("该用户名已经被注册！");
        }
        int i = personDao.addPerson(person);
        if (i!=1) {
            throw new RegistFaildException("注册用户失败！");
        }
    }

    @Override
    public Person login(String personName, String password) {
        System.out.println("业务层中的登录方法。。。。。");
        if (StringUtils.isEmpty(personName) || StringUtils.isEmpty(password)) {
            throw new IllegalParameterException("用户名密码不能为空！");
        }
        Person person = personDao.findPersonByName(personName);

        if (person==null) {
            throw new UserNotFoundException("该用户不存在！");
        }
        if (!person.getPassword().equals(password)) {
            throw new PasswordErrorException("密码错误！");
        }
        return person;
    }

    @Override
    public List<Person> list() {
        System.out.println("调用list获取列表");
        List<Person> list = personDao.findPersonList();
//        list=null;
        if (list==null || list.size()==0){
            throw new PersonListIsEmptyException("查询列表为空异常！");
        }
        return list;
    }

    @Override
    public void deleteById(Integer id) {
        if (id==null ||id==0)
            throw new IllegalParameterException("删除的id参数异常！");
        Person person = personDao.findPersonById(id);
        if (person==null)
            throw new UserNotFoundException("该账号不存在，不能删除！");
        int i = personDao.deleteById(id);
        if (i!=1)
            throw new DeleteFaildException("删除person失败！");
    }
}
