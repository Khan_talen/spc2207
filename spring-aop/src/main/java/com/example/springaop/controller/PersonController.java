package com.example.springaop.controller;

import com.example.springaop.entity.Person;
import com.example.springaop.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    PersonService personService;

    @PostMapping("/regist")
    public String regist(Person person){
        personService.regist(person);
        return "注册成功！";
    }

    @PostMapping("/login")
    public String login(String personName,String password){
        Person user = personService.login(personName,password);
        System.out.println(user);
        return "登录成功！";
    }

    @GetMapping("/list")
    public List<Person> list(){
        return personService.list();
    }

    @GetMapping("/deletePersonById")
    public String deletePersonById(Integer id){
        personService.deleteById(id);
        return "删除成功！";
    }

}
