package com.example.springaop.dao;


import com.example.springaop.entity.Person;

import java.util.List;

public interface PersonDao {
    Person findPersonByName(String name);

    int addPerson(Person person);

    List<Person> findPersonList();

    int deleteById(Integer id);

    Person findPersonById(Integer id);
}
