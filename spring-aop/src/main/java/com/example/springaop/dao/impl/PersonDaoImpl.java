package com.example.springaop.dao.impl;

import com.example.springaop.dao.PersonDao;
import com.example.springaop.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonDaoImpl implements PersonDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    RowMapper<Person> rowMapper = (resultSet, rownum)->{
        return new Person(resultSet.getInt("id"),resultSet.getString("name"),resultSet.getString("password"));
    };

    @Override
    public Person findPersonByName(String name) {
        String sql = "select * from person where name=?";
        Person person = null;
        try{
            person = jdbcTemplate.queryForObject(sql,rowMapper,name);
        }catch (EmptyResultDataAccessException e){
            return null;
        }
        return person;
    }

    @Override
    public int addPerson(Person person) {
        String sql = "insert into person values(null,?,?)";
        return jdbcTemplate.update(sql,person.getName(),person.getPassword());
    }

    @Override
    public List<Person> findPersonList() {
        String sql = "select * from person";
        return jdbcTemplate.query(sql,rowMapper);
    }

    @Override
    public int deleteById(Integer id) {
        String sql = "delete from person where id=?";
        return jdbcTemplate.update(sql,id);
    }

    @Override
    public Person findPersonById(Integer id) {
        String sql = "select * from person where id=?";
        Person person = null;
        try {
            jdbcTemplate.queryForObject(sql, rowMapper, id);
        }catch (EmptyResultDataAccessException e){
            return null;
        }
        return person;
    }
}
