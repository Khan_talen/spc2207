package com.example.springtest.service;

import com.example.springtest.dao.UserDao;
import com.example.springtest.entity.User;
import com.example.springtest.exception.IllegalParameterException;
import com.example.springtest.exception.RegistFaildException;
import com.example.springtest.exception.UserNameIsUsedException;
import com.example.springtest.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UserServiceJunitTest {

    @Test
    public void registTest(){
        //模拟UserDao对象
        UserDao userDao = mock(UserDao.class);
        //创建UserServiceImpl实例
        UserServiceImpl userService = new UserServiceImpl();
        userService.setUserDao(userDao);   //注入模拟对象

        //匿名颞部类实现Executable接口
        assertThrows(IllegalParameterException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                userService.regist(null,"123");
            }
        });

        //测试用户名/密码为空时，是否抛出指定异常
        RuntimeException exception = assertThrows(IllegalParameterException.class,
                ()->userService.regist(null,"123"));
        assertEquals("参数异常！",exception.getMessage());
        //自行测试用户名不为空，密码为空，是否抛出指定异常；测试用户名密码均为空，是否抛出指定异常

        //测试注册的用户名已存在情况，是否抛出异常 -- 当注册的用户名为tom时，假设用户已存在，此时进行测试
        //因为这里涉及到调用持久层的方法，需要根据其返回值进行判断，所以需要对UserDao的对应方法进行训练
        when(userDao.findUserByName("tom")).thenReturn(new User());
        exception = assertThrows(UserNameIsUsedException.class,
                ()->userService.regist("tom","123"));
        assertEquals("该用户名已经被注册！",exception.getMessage());
        //测试注册失败的情况，是否抛出异常
        when(userDao.addUser(any(User.class))).thenReturn(0);
        exception = assertThrows(RegistFaildException.class,
                ()->userService.regist("lisa","123"));
        assertEquals("注册用户失败！",exception.getMessage());
        //测试注册成功,返回的User对象不为空
        when(userDao.addUser(any(User.class))).thenReturn(1);
        assertDoesNotThrow(()->{
            assertNotNull(userService.regist("jack","123"));
        });
    }
}
