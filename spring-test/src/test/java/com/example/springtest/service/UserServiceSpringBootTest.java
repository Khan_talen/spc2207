package com.example.springtest.service;

import com.example.springtest.exception.IllegalParameterException;
import com.example.springtest.exception.PasswordErrorException;
import com.example.springtest.exception.UserNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 集成测试，测试业务层+持久层，需要Spring环境
 */
@SpringBootTest
public class UserServiceSpringBootTest {

    @Autowired
    UserService userService;

    //测试上下文对象有几个
    @Test
    @DirtiesContext
    public void test1(){
        System.out.println(userService);
    }
    @Test
    public void test2(){
        System.out.println(userService);
    }

    @Test
    public void loginTest(){
        //测试用户名或密码为空，是否抛出指定异常
        RuntimeException exception = assertThrows(IllegalParameterException.class,()->userService.login(null,"123"));
        assertEquals("用户名密码不能为空！",exception.getMessage());
        //测试用户名不存在的情况，是否抛出指定异常
        exception = assertThrows(UserNotFoundException.class,()->userService.login("amy","123"));
        assertEquals("该用户不存在！",exception.getMessage());
        //测试密码错误的情况，是否抛出指定异常
        exception = assertThrows(PasswordErrorException.class,()->userService.login("tom","222"));
        assertEquals("密码错误！",exception.getMessage());
        //测试登录成功，返回的User对象中的值是否是指定值
        assertDoesNotThrow(()->assertNotNull(userService.login("tom","123")));

    }
}
