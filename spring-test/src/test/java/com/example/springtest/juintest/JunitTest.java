package com.example.springtest.juintest;

import com.example.springtest.junitdemo1.MyService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Junit5的常用测试注解
 */
public class JunitTest {
    MyService myService;

    @BeforeAll
    public static void init(){
        System.out.println("init");
    }

    @AfterAll
    public static void des(){
        System.out.println("destory");
    }

    @BeforeEach
    public void serviceInstance(){
        myService = new MyService();
        System.out.println("实例化service");
    }

    @Test
    public void registTest(){
        System.out.println("测试注册");
        boolean result = myService.regist();
        assertTrue(result);
    }

    @Test
    public void addTest(){
        System.out.println("测试加法");
        int result = myService.add(3,4);
//        System.out.println(result);
        assertEquals(8,result);
    }

    @Test
    public void minusTest(){
        System.out.println("测试减法");
        int result = myService.minus(4,3);
        System.out.println(result);
    }

    @Test
    public void productTest(){
        System.out.println("测试乘法");
        int result = myService.product(2,3);
        System.out.println(result);
    }

    @Test
    public void diviseTest(){
        System.out.println("测试除法");
        int result = myService.division(10,2);
        System.out.println(result);
    }


}
