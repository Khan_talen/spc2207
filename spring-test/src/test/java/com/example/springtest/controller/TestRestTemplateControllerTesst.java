package com.example.springtest.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * 使用TestRestTemplate进行web集成测试 -- 启动web容器
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestRestTemplateControllerTesst {

    @Autowired
    TestRestTemplate template;

    @Test
    public void registTest(){
        //定义相对路径
        String url = "/users/regist";
        //post请求传参，参数必须是MultiValueMap类型
        MultiValueMap<String,Object> map = new LinkedMultiValueMap<>();
        map.add("userName","张三");
        map.add("pwd","123");
        //发起post请求
        String mes = template.postForObject(url,map,String.class);
        assertEquals("注册成功！",mes);

    }

    @Test
    public void loginTest(){
        //定义请求的相对路径
        //get请求传参是在请求后通过？拼接参数
        String url = "/users/login?userName={username}&pwd={password}";
        String mes = template.getForObject(url,String.class,"tom","123");
        assertEquals("登录成功！",mes);
    }


}
