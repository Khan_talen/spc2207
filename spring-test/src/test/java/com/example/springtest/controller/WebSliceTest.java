package com.example.springtest.controller;

import com.example.springtest.entity.User;
import com.example.springtest.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * WEB切片测试，仅用于测试控制器
 *
 */
@WebMvcTest(UserController.class)
public class WebSliceTest {

    @Autowired
    MockMvc mockMvc;

    //模拟UserService对象
    @MockBean
    UserService userService;
    @Test
    public void registTest() throws Exception {
        //训练UserService，调用regist方法，返回User对象
        given(userService.regist("王五","123"))
                .willReturn(new User(10,"王五","123"));
        //模拟发起post请求
        MultiValueMap<String,String> map = new LinkedMultiValueMap<>();
        map.add("userName","王五");
        map.add("pwd","123");
        mockMvc.perform(post("/users/regist").params(map))
                .andExpect(status().isOk())
                .andExpect(content().string("注册成功！"))
                .andDo(print());
    }
}
