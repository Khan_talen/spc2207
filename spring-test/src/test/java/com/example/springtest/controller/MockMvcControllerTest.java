package com.example.springtest.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * MOCKMVC测试，不需要启动web容器，模拟发起请求测试
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class MockMvcControllerTest {

    @Autowired
    MockMvc mockMvc;

    /**
     * mock发起get请求
     * @throws Exception
     */
    @Test
    public void loginTest() throws Exception {
        String url = "/users/login?userName={userName}&pwd={password}";
        mockMvc.perform(get(url,"tom","123"))
                .andExpect(status().isOk())
                .andExpect(content().string("登录成功！"))
                .andDo(print());

    }

    /**
     * mock 发起post请求并涉及到传递多个参数
     * @throws Exception
     */
    @Test
    public void registTest() throws Exception {
        //通过MultiValueMap保存多个参数，使用params传递map
        MultiValueMap<String,String> map = new LinkedMultiValueMap<>();
        map.add("userName","李四");
        map.add("pwd","1234");

        mockMvc.perform(post("/users/regist").params(map))
                .andExpect(status().isOk())
                .andExpect(content().string("注册成功！"))
                .andDo(print());
    }



}
