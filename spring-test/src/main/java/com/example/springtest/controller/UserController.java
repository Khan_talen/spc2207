package com.example.springtest.controller;

import com.example.springtest.entity.User;
import com.example.springtest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;


    @PostMapping("/regist")
    public String regist(String userName,String pwd){
        userService.regist(userName,pwd);
        return "注册成功！";
    }

    @GetMapping("/login")
    public String login(String userName, String pwd, HttpSession session){
        User user = userService.login(userName,pwd);
        session.setAttribute("user",user);
        return "登录成功！";
    }

}
