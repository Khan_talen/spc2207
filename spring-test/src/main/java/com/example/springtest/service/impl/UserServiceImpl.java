package com.example.springtest.service.impl;

import com.example.springtest.dao.UserDao;
import com.example.springtest.entity.User;
import com.example.springtest.exception.*;
import com.example.springtest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

//    @Autowired
    UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User regist(String username, String password) {
        if (username==null || password==null) {
            throw new IllegalParameterException("参数异常！");
        }

        User findUser = userDao.findUserByName(username);
        if (findUser!=null) {
            throw new UserNameIsUsedException("该用户名已经被注册！");
        }
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        int i = userDao.addUser(user);
        if (i!=1) {
            throw new RegistFaildException("注册用户失败！");
        }
        return user;
    }

    @Override
    public User login(String username, String password) {
        if (username==null || password == null) {
            throw new IllegalParameterException("用户名密码不能为空！");
        }
        User user = userDao.findUserByName(username);

        if (user==null) {
            throw new UserNotFoundException("该用户不存在！");
        }
        if (!user.getPassword().equals(password)) {
            throw new PasswordErrorException("密码错误！");
        }
        return user;
    }

    @Override
    public List<User> list() {
        return userDao.findAllUser();
    }

    @Override
    public User getById(Integer id) {
        return userDao.findUserById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userDao.findUserByName(username);
    }

    @Override
    public void update(User user) {
        int n = userDao.updateUser(user);
        if (n != 1){
            throw new UpdateFaildException("更新失败！");
        }
    }

    @Override
    public void delete(User user) {
        int n = userDao.deleteUser(user.getId());
        if (n != 1){
            throw new DeleteFaildException("更新失败！");
        }
    }
}
