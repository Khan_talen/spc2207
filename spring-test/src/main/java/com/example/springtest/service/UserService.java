package com.example.springtest.service;

import com.example.springtest.entity.User;

import java.util.List;

public interface UserService {

    User regist(String userName, String pwd);

    User login(String userName, String pwd);

    List<User> list();

    User getById(Integer id);

    User getByUsername(String username);

    void update(User user);

    void delete(User user);
}
