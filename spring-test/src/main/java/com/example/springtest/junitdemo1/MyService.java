package com.example.springtest.junitdemo1;

public class MyService {

    public int add(int num1,int num2){
        return num1+num2;
    }

    public int minus(int num1,int num2){
        return num1-num2;
    }

    public int product(int num1,int num2){
        return num1*num2;
    }

    public int division(int num1,int num2){
        return num1/num2;
    }

    public String login(){
        return "登录成功！";
    }

    public boolean regist(){
        return true;
    }
}
