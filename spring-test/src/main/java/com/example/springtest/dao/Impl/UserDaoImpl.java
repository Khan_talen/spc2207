package com.example.springtest.dao.Impl;

import com.example.springtest.dao.UserDao;
import com.example.springtest.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public int addUser(User user) {
        String sql = "insert into user (username,password) values (?,?)";
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        PreparedStatementCreator preparedStatementCreator = con -> {
            PreparedStatement ps = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            return ps;
        };
        int num = jdbcTemplate.update(preparedStatementCreator, keyHolder);
        user.setId(keyHolder.getKey().intValue());
        return num;
    }

    @Override
    public int updateUser(User user) {
        String sql = "update user set username=?, password=? where id=?";
        return jdbcTemplate.update(sql, user.getUsername(), user.getPassword(), user.getId());
    }


    @Override
    public int deleteUser(Integer id) {
        String sql = "delete from where id=?";
        return jdbcTemplate.update(sql, id);
    }


    @Override
    public List<User> findAllUser() {
        String sql = "select * from user";
        return jdbcTemplate.query(sql, rowMapper);
    }

    private RowMapper<User> rowMapper = (rs, index)->{
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setUsername(rs.getString("username"));
        user.setPassword(rs.getString("password"));
        return user;
    };

    @Override
    public User findUserById(Integer id) {
        String sql = "select * from user where id=?";
        try {
            return jdbcTemplate.queryForObject(sql, rowMapper);
        }catch (EmptyResultDataAccessException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public User findUserByName(String userName) {
        String sql = "select * from user where username=?";
        try {
            return jdbcTemplate.queryForObject(sql, rowMapper, userName);
        }catch (EmptyResultDataAccessException e){
            return null;
        }
    }
}
