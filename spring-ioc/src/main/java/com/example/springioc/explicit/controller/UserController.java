package com.example.springioc.explicit.controller;


import com.example.springioc.explicit.mapper.UserMapper;

/**
 * 用于实验显式配置Bean
 */
public class UserController {

    /**
     * 参数注入时放开
     */

    public UserMapper userMapper;

    public UserController(UserMapper userMapper){
        this.userMapper = userMapper;
    }

}
