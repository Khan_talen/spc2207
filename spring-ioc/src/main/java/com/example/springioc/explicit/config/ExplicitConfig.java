package com.example.springioc.explicit.config;

import com.example.springioc.explicit.controller.UserController;
import com.example.springioc.explicit.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * 显示配置Bean的配置类
 */
@Configuration
public class ExplicitConfig {
    /**
     * 显示配置Bean
     *
     */
//    @Bean
//    public UserController userController(){
//        return new UserController();
//    }

    @Bean
    public UserMapper userMapper(){
        return new UserMapper();
    }



    /**
     * 注入参数
     * @return
     */
    @Bean
//    @Scope("prototype")
    public UserController userController(UserMapper userMapper){
        return new UserController(userMapper);
    }



}
