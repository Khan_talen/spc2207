package com.example.springioc.implicit.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *  隐式配置Bean的配置类
 */
@Configuration
@ComponentScan("com.example.springioc.implicit")
public class ImplicitConfig {

}
