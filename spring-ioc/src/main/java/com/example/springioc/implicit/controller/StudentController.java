package com.example.springioc.implicit.controller;

import com.example.springioc.implicit.mapper.StudentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

/**
 * 用于实验隐式配置，以及@Autowired注入方式
 */
//@Component("stu")
@Scope("prototype")
@Controller("student")
public class StudentController {
    @Autowired
    public StudentMapper studentMapper;

    /**
     * 构造方法注入
     */
//    @Autowired
//    public StudentController(StudentMapper studentMapper){
//        this.studentMapper = studentMapper;
//    }

    /**
     * setter注入
     */
//    @Autowired
//    public void setStudentMapper(StudentMapper studentMapper){
//        this.studentMapper = studentMapper;
//    }

}
