package com.example.springioc.autowiredquestion.controller;

import com.example.springioc.autowiredquestion.mapper.PersonMapper;
import com.example.springioc.autowiredquestion.mapper.WorkerMapper;
import com.example.springioc.autowiredquestion.mapper.impl.CatMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

/**
 * 用于实验某个类型的依赖项存在多个时，产生歧义的解决
 * 案例步骤：
 * 1. 在worker系列的controller和mapper实现类上方添加组件类注解
 * 2. 在WorkerController中注入WorkerMapper类型的实例、
 * 3. 在测试类启动spring容器，观察是否启动成功 - 启动失败正确
 *    -- 这里启动失败，因为注入的时候产生了歧义
 * 4. 解决办法，在注入的位置添加注入@Qualifier指定beanId，再次测试，注入成功
 */
@Controller
public class WorkerController {

    /*
    注入的依赖项不存在,会发生什么?
        启动spring容器失败
        若想实现有依赖项,则注入,没有依赖项则不注入,不报错,可以使用属性required=false实现
     */
    @Autowired(required = false)
    public PersonMapper personMapper;

    /**
     * 构造方法注入CatMapper实例
     */
    public CatMapper catMapper;

    //set方法注入
    @Autowired
    public void setCatMapper(CatMapper catMapper){
        this.catMapper = catMapper;
    }


//    @Autowired
//    public WorkerController(CatMapper catMapper){
//        this.catMapper = catMapper;
//    }



//    @Autowired
//    @Qualifier("workerMapperImpl1")
//    public WorkerMapper workerMapper;

    /**
     * 构造注入中@Qualifier注解的用法
     */
//    @Autowired
//    public WorkerController(@Qualifier("workerMapperImpl2") WorkerMapper workerMapper){
//        this.workerMapper = workerMapper;
//    }

    /**
     * setter注入，使用@Qualifier消除歧义
     */

//    @Autowired
//    public void setMapper(@Qualifier("workerMapperImpl2") WorkerMapper workerMapper){
//        this.workerMapper = workerMapper;
//    }

    /**
     * 消除歧义的其他办法，若产生歧义，且没有@Qualifier,则自动根据beanId查找
     */

//    @Autowired
//    public   WorkerMapper workerMapperImpl1;

    /**
     * 实验@Autowired sette注入中若根据类型匹配产生歧义，根据beanid去查，
     * beanId是由方法名得出，还是参数名得出
     */
    public WorkerMapper workerMapper;

    @Autowired
    public void setWorkerMapper(WorkerMapper workerMapperImpl1){
        this.workerMapper = workerMapperImpl1;
    }
}
