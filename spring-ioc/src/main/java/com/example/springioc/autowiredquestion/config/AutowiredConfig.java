package com.example.springioc.autowiredquestion.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 演示Autowired注入产生问题的配置类
 */
@Configuration
@ComponentScan("com.example.springioc.autowiredquestion")
public class AutowiredConfig {
}
