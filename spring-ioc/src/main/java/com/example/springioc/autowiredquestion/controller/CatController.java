package com.example.springioc.autowiredquestion.controller;

import com.example.springioc.autowiredquestion.mapper.impl.CatMapper;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

/**
 * 用于实验@Resource注解的用法
 */
@Controller
public class CatController {
    /**
     * 试验字段注入，若name不匹配，是否能根据类型注入
     */
//    @Resource
//    public CatMapper cat;

    /**
     * setter注入
     * 1. 将参数名不与beanId对应，看是否注入成功，确定beanname在方法名上
     * 2. 将方法名改了，不与beanId对应，看是否注入成功，确定是否会回退到类型注入
     */
    public CatMapper catMapper;

    @Resource
    public void setCatMappe(CatMapper cat){
        this.catMapper = cat;
    }

    /**
     * 实验构造注入  -- 该注解不用于构造注入
     */
//    @Resource
//    public CatController(CatMapper catMapper){
//        this.catMapper = catMapper;
//    }
}
