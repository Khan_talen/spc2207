package com.example.springioc.autowiredquestion.controller;

import com.example.springioc.autowiredquestion.mapper.PersonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * 用于实验找不到匹配类型的Bean依赖，即某接口没有实现类
 */
@Controller
public class PersonController {
    @Autowired(required = false)
    public PersonMapper personMapper;

}
