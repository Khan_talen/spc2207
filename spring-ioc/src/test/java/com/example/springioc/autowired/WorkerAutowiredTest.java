package com.example.springioc.autowired;

import com.example.springioc.autowiredquestion.config.AutowiredConfig;
import com.example.springioc.autowiredquestion.controller.WorkerController;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;

/**
 * @Autowired注入的依赖项存在多个，产生歧义的测试
 */
public class WorkerAutowiredTest {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(AutowiredConfig.class);
        //获取WorkerController实例，并输出WorkerMapper
        WorkerController controller = context.getBean(WorkerController.class);

//        System.out.println(controller.personMapper);

//        System.out.println(controller.catMapper);
//        System.out.println(controller.workerMapper);

//        System.out.println(controller.workerMapperImpl1);

        System.out.println(controller.workerMapper);

    }
}
