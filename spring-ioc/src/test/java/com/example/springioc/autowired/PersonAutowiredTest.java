package com.example.springioc.autowired;

import com.example.springioc.autowiredquestion.config.AutowiredConfig;
import com.example.springioc.autowiredquestion.controller.PersonController;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

/**
 * @Autowired存在的问题，若依赖项不存在，会发生什么？
 * 案例步骤：
 *  1. 配置类，controller上方添加必要的注解
 *  2. 在controller中注入PersonMapper(是接口，没有实现类)
 *  3. 测试类从上下文对象获取controller，启动观察结果
 *  4. 在注入PersonMapper时，添加属性required=false，再次运行观察结果
 */
public class PersonAutowiredTest {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(AutowiredConfig.class);
        //获取PersonControllerBean对象
        PersonController controller = context.getBean(PersonController.class);
        System.out.println(controller.personMapper);

    }
}
