package com.example.springioc.autowired;

import com.example.springioc.autowiredquestion.config.AutowiredConfig;
import com.example.springioc.autowiredquestion.controller.CatController;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

/**
 * @Resource注解测试
 *  1. 测试2种用法
 *      - 注意，setter注入的name在哪里
 *  2. 若不存在name，则自动回退到根据类型注入
 */
public class ResourceTest {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(AutowiredConfig.class);
        CatController controller = context.getBean(CatController.class);
//        System.out.println(controller.cat);
        System.out.println(controller.catMapper);
    }

}
