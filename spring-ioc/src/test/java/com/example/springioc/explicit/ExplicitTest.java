package com.example.springioc.explicit;

import com.example.springioc.explicit.config.ExplicitConfig;
import com.example.springioc.explicit.controller.UserController;
import com.example.springioc.explicit.mapper.UserMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

/**
 * 显示配置Bean测试
 */
public class ExplicitTest {
    public static void main(String[] args) {
        /**
         * ApplicationContext 表示应用程序上下文对象 -- Spring容器
         * 上下文对象作用于程序的整个生命周期
         * 即Spring项目启动，得到该对象，一直会存在整个生命周期中，即项目运行着，
         * 这个对象一直存在
         *
         * 得到Spring容器，即上下文对象
         */

        ApplicationContext context = SpringApplication.run(ExplicitConfig.class);
        //获取UserController对象-通过BeanId获取
//        UserController userController = (UserController) context.getBean("userController");
//        System.out.println(userController);
//        //获取UserMapper对象 -- 通过BeanId获取Bean对象，且同时指定最终的类型
//        UserMapper userMapper = context.getBean("userMapper", UserMapper.class);
//        System.out.println(userMapper);

        //通过数据类型分别获取UserController类型的Bean和UserMapper类型的Bean
//        UserController controller1 = context.getBean(UserController.class);
//        UserMapper userMapper1 = context.getBean(UserMapper.class);
//        System.out.println(controller1);
//        System.out.println(userMapper1);

        /**
         * 参数注入问题
         */
        //@Bean显式配置的参数注入是否成功，获取controllerBean，检查其中的属性值是否不为null
        UserController controller = context.getBean(UserController.class);
        System.out.println(controller.userMapper);

        /**
         * 测试添加了@Scope注解的UserController是否称为原型
         */
//        UserController controller1 = context.getBean(UserController.class);
//        UserController controller2 = context.getBean(UserController.class);
//        UserController controller3 = context.getBean(UserController.class);
//        System.out.println(controller1);
//        System.out.println(controller2);
//        System.out.println(controller3);
    }
}
