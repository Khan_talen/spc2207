package com.example.springioc.implicit;

import com.example.springioc.implicit.config.ImplicitConfig;
import com.example.springioc.implicit.controller.StudentController;
import com.example.springioc.implicit.mapper.StudentMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

/**
 * 隐式配置测试类
 */
public class ImplicitTest {
    public static void main(String[] args) {
        //得到上下文对象
        ApplicationContext context = SpringApplication.run(ImplicitConfig.class);
        //从上下文对象中获取Bean对象--通过类型获取Bean对象
//        StudentController controller = context.getBean(StudentController.class);
//        StudentMapper studentMapper = context.getBean(StudentMapper.class);
//
//        System.out.println(controller);
//        System.out.println(studentMapper);

        //通过beanId获取Bean对象
//        StudentController controller = context.getBean("student",StudentController.class);
//        System.out.println(controller);

        /**
         * 从上下文对象中多次获取同一个类型的Bean对象，获取到的是多个Bean吗？
         * 作用域问题
         */
        StudentController controller1 = context.getBean(StudentController.class);
        StudentController controller2 = context.getBean(StudentController.class);
        StudentController controller3 = context.getBean(StudentController.class);
        System.out.println(controller1);
        System.out.println(controller2);
        System.out.println(controller3);
    }
}
