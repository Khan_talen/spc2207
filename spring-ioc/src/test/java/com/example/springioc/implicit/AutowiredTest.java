package com.example.springioc.implicit;

import com.example.springioc.implicit.config.ImplicitConfig;
import com.example.springioc.implicit.controller.StudentController;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

/**
 * @Autowired注解注入方式测试
 */
public class AutowiredTest {
    public static void main(String[] args) {
        /**
         * 获取StudentController对象，检查属性studentMapper是否注入对象成功
         */
//        ApplicationContext context = SpringApplication.run(
//                ImplicitConfig.class);
//        StudentController controller = context.getBean(
//                StudentController.class);
//        System.out.println(controller.studentMapper);
    }
}
